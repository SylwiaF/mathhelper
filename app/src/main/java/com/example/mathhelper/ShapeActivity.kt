package com.example.mathhelper

import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.example.mathhelper.R.id.best_shape
import com.example.mathhelper.models.ShapeFactory

class ShapeActivity : AbstractActivity() {

    private lateinit var actualScore: TextView
    private lateinit var bestScore: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shape)

        val drawing = findViewById<ImageView>(R.id.drawing_shape)
        val taskText = findViewById<TextView>(R.id.text_shape)
        val firstFactor = findViewById<TextView>(R.id.first_factor_shape)
        val secondFactor = findViewById<TextView>(R.id.second_factor_shape)

        val userChoice =  intent.getStringExtra("shape")

        val shape = ShapeFactory().build(userChoice)

        counter = intent.getIntExtra("counter", 0)

        actualScore = findViewById(R.id.actual_shape)
        actualScore.text = counter.toString()
        bestScore = findViewById(best_shape)
        bestScore.text = score.maxResult()

        drawing.setImageResource(shape.getDrawing())
        taskText.text = shape.getTaskText()
        firstFactor.text = shape.getFirstFactor()
        secondFactor.text = shape.getSecondFactor()

        buttonList = arrayListOf(
            findViewById(R.id.button1_shape),
            findViewById(R.id.button2_shape),
            findViewById(R.id.button3_shape),
            findViewById(R.id.button4_shape)
        )
        buttonList.shuffle()
        buttonList[0].text = shape.getSolution().toString()
        for (i in 1 until buttonList.size) {
            "${shape.getAnswers()[i - 1]}".also { buttonList[i].text = it }
        }

        setOnClickListeners()

        findViewById<Button>(R.id.next_shape).setOnClickListener {
            finish()
            intent.putExtra("counter", counter)
            startActivity(intent)
        }

    }
}