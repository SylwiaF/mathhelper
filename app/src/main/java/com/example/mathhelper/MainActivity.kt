package com.example.mathhelper

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import java.io.FileOutputStream

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        val ten = findViewById<Button>(R.id.ten_menu)
        val twenty = findViewById<Button>(R.id.twenty_menu)
        val thirty = findViewById<Button>(R.id.thirty_menu)
        val fifty = findViewById<Button>(R.id.fifty_menu)
        val eighty = findViewById<Button>(R.id.eighty_menu)
        val hundred = findViewById<Button>(R.id.hundred_menu)

        val oneHundred = findViewById<Button>(R.id.one_hundred_menu)
        val oneThousand = findViewById<Button>(R.id.one_thousand_menu)

        val square = findViewById<Button>(R.id.squere_menu)
        val triangle = findViewById<Button>(R.id.triangle_menu)
        val rectangle = findViewById<Button>(R.id.rectangle_menu)

        var value: String
        val valueIntermediateList = arrayListOf(oneHundred, oneThousand)
        val valueBasicList = arrayListOf(ten, twenty, thirty, fifty, eighty, hundred)
        val shapesList = arrayListOf(square, triangle, rectangle)

        //przejści do zadań średniozaawnsowanych
        for (i in 0 until valueIntermediateList.size) {
            valueIntermediateList[i].setOnClickListener {
                value = valueIntermediateList[i].text.toString()
                val intent = Intent(this, FigureActivity::class.java)
                    .putExtra("range", value.toInt())
                    .putExtra("operations", "all")
                startActivity(intent)
            }
        }

        //przejście do zadań podstawowych z zakresu 10, 20 .. do 100
        for (i in 0 until valueBasicList.size) {
            valueBasicList[i].setOnClickListener {
                value = valueBasicList[i].text.toString()
                val intent = Intent(this, FigureActivity::class.java)
                    .putExtra("range", value.toInt())
                    .putExtra("operations", "+-")
                startActivity(intent)
            }
        }

        //przejście do zadań związanych z figurami
        for (i in 0 until shapesList.size) {
            shapesList[i].setOnClickListener {
                value = shapesList[i].text.toString()
                val intent = Intent(this, ShapeActivity::class.java).putExtra("shape", value)
                startActivity(intent)
            }
        }

    }
}