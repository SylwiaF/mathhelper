package com.example.mathhelper.models

import com.example.mathhelper.shape.Triangle

interface TriangleTaskInterface {
    fun fill(item: Triangle)
}