package com.example.mathhelper.models

import com.example.mathhelper.shape.Square

interface SquareTaskInterface {
    fun fill(item: Square)
}