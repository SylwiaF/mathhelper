package com.example.mathhelper.models

import kotlin.math.sqrt
import kotlin.random.Random

class ShapeCommons {
    // Zwraca losową liczbę naturalną, która po podniesieniu do kwadratu mieści się w zakresie rangeMin, rangeMax
    fun getRandomPow(rangeMin: Int, rangeMax: Int): Int {
        val factorRangeMax = sqrt(rangeMax.toDouble()).toInt()
        val factorRangeMin = sqrt(rangeMin.toDouble()).toInt()
        return Random.nextInt(factorRangeMin, factorRangeMax)
    }

    fun randomPythagoreanTriplets(limit: Int, aParity: Boolean = false): PythagoreanTriplet {

        val results = arrayListOf<PythagoreanTriplet>()

        // wzór: a^2 + b^2 = c^2
        var a: Int
        var b: Int
        var c = 0

        var m = 2

        while (c < limit) {

            for (n in 1 until m) {
                a = m * m - n * n
                b = 2 * m * n
                c = m * m + n * n
                if (c > limit) break

                if (!aParity || a % 2 == 0) {
                    results.add(PythagoreanTriplet(a, b, c))
                }
            }
            m++
        }
        results.shuffle()
        return results[0]
    }

    fun buildAnswers(shape: ShapeInterface, amountOfAnswers: Int) {
        val minVal = if (shape.getSolution() < 10) 0 else shape.getSolution() - 10
        val answers: MutableList<Int> = ArrayList()
        var randomValue: Int
        for (i in 0 until amountOfAnswers) {
            do {
                randomValue = Random.nextInt(minVal, shape.getSolution() + 10)
            } while (randomValue == shape.getSolution() || answers.contains(randomValue))

            answers.add(randomValue)
        }

        shape.setAnswers(answers)
    }

}
