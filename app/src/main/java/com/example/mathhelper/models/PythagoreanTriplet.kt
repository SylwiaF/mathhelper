package com.example.mathhelper.models

data class PythagoreanTriplet(
    var a: Int = 0,
    var b: Int = 0,
    var c: Int = 0 // Przeciwprostokątna
)
