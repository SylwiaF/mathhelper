package com.example.mathhelper.models

import com.example.mathhelper.shape.Rectangle
import com.example.mathhelper.shape.Square
import com.example.mathhelper.shape.Triangle

class ShapeFactory {
    fun build(shape: String?): ShapeInterface {
        return when (shape) {
            "Kwadrat" -> Square().generateTask()
            "Prostokąt" -> Rectangle().generateTask()
            "Trójkąt" -> Triangle().generateTask()
            else -> Square().generateTask()
        }
    }
}