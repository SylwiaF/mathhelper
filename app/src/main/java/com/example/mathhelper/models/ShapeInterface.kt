package com.example.mathhelper.models

interface ShapeInterface {

    fun getDrawing(): Int

    fun getFirstFactor(): String

    fun getSecondFactor(): String

    fun getTaskText(): String

    fun getSolution(): Int

    fun getAnswers(): MutableList<Int>

    fun setAnswers(answers: MutableList<Int>)

    fun generateTask(): ShapeInterface
}