package com.example.mathhelper.models

data class MathOperation(
    var x: Int? = null,
    var y: Int? = null,
    var operator: String,
    var solution: Int = 0,
    var answers: IntArray = IntArray(3)
)