package com.example.mathhelper.models

import com.example.mathhelper.shape.Rectangle

interface RectangleTaskInterface {
    fun fill(item: Rectangle)
}