package com.example.mathhelper.models

import android.content.Context
import com.example.mathhelper.File

class Score (var context: Context) {

    fun checkScore(actualResult: String){
        val file = File(context)
        val bestResult = file.fileRead()

        if (bestResult.toInt() < actualResult.toInt()){
            file.fileSave(actualResult)
        }


    }
    fun maxResult(): String {
        val file = File(context)
        return file.fileRead()
    }
}