package com.example.mathhelper.number

import com.example.mathhelper.models.MathOperation
import kotlin.random.Random

class BuildAnswers() {

    fun build(item: MathOperation): MathOperation {
        item.answers.forEachIndexed { index, _ ->
            item.answers[index] = generateAnswer(index, item)
        }
        return item
    }

    private fun generateAnswer(iteration: Int, item: MathOperation): Int {
        var value: Int
        val minValue = if (item.solution - 10 < 0) 0 else item.solution - 10
        do {
            value = Random.nextInt(minValue, item.solution + 10)
        } while (!checkIfExists(value, iteration, item))
        return value
    }

    private fun checkIfExists(value: Int, iteration: Int, item: MathOperation): Boolean {
        if (value == item.solution) return false
        if (iteration == 0) return true
        for (i in 0 until iteration) {
            if (item.answers[i] == value) return false
        }
        return true
    }

}