package com.example.mathhelper.number

import com.example.mathhelper.models.MathOperation

class OperationGenerator(val range: Int) {

    private var operationsMatrix: MutableList<String>

    init {
        operationsMatrix = when (range) {
            100 -> arrayListOf("+", "-", "*", ":", "^2", "√")
            1000 -> arrayListOf("+", "-", "*", ":", "^3", "∛")
            else -> arrayListOf("")
        }
    }

    fun generate(): MathOperation {
        operationsMatrix.shuffle()
        var result = MathOperation(operator = operationsMatrix[0])

        val casting = Casting()
        result = casting.castNumbers(result, range)

        val answerBuilder = BuildAnswers()
        result = answerBuilder.build(result)

        return result
    }

    fun setOperationMatrix(list: MutableList<String>) {
        operationsMatrix = list
    }
}