package com.example.mathhelper.number

import com.example.mathhelper.models.MathOperation
import java.lang.Math.cbrt
import kotlin.math.sqrt
import kotlin.random.Random

class Casting {

    fun castNumbers(item: MathOperation, range: Int): MathOperation {
        return when (item.operator) {
            "+" -> castAdd(item, range)
            "-" -> castSub(item, range)
            "*" -> castMult(item, range)
            ":" -> castDiv(item, range)
            "^2" -> castPow2(item, range)
            "^3" -> castPow3(item, range)
            "√" -> castSquare(item, range)
            "∛" -> castCubic(item, range)
            else -> item
        }
    }

    private fun castAdd(item: MathOperation, range: Int): MathOperation {
        item.solution = Random.nextInt(2, range)
        val x = Random.nextInt(1, item.solution)
        item.x = x
        item.y = item.solution - x
        return item
    }

    private fun castSub(item: MathOperation, range: Int): MathOperation {
        item.solution = Random.nextInt(0, range - 1)
        val x = Random.nextInt(item.solution, range)
        item.x = x
        item.y = x - item.solution
        return item
    }

    private fun castMult(item: MathOperation, range: Int): MathOperation {
        val factorRange = sqrt(range.toDouble()).toInt()
        val x = Random.nextInt(1, factorRange)
        val y = Random.nextInt(1, factorRange)
        item.x = x
        item.y = y
        item.solution = x * y
        return item
    }

    private fun castDiv(item: MathOperation, range: Int): MathOperation {
        val factorRange = sqrt(range.toDouble()).toInt()
        item.solution = Random.nextInt(1, factorRange)
        val y = Random.nextInt(1, factorRange)
        item.x = item.solution * y
        item.y = y
        return item
    }

    private fun castPow2(item: MathOperation, range: Int): MathOperation {
        val factorRange = sqrt(range.toDouble()).toInt()
        val x = Random.nextInt(1, factorRange)
        item.x = x
        item.y = null
        item.solution = x * x
        return item
    }

    private fun castPow3(item: MathOperation, range: Int): MathOperation {
        val factorRange = cbrt(range.toDouble()).toInt()
        val x = Random.nextInt(1, factorRange)
        item.x = x
        item.y = null
        item.solution = x * x * x
        return item
    }

    private fun castSquare(item: MathOperation, range: Int): MathOperation {
        val factorRange = sqrt(range.toDouble()).toInt()
        item.solution = Random.nextInt(1, factorRange)
        item.x = null
        item.y = item.solution * item.solution
        return item
    }

    private fun castCubic(item: MathOperation, range: Int): MathOperation {
        val factorRange = cbrt(range.toDouble()).toInt()
        item.solution = Random.nextInt(1, factorRange)
        item.x = null
        item.y = item.solution * item.solution
        return item
    }
}