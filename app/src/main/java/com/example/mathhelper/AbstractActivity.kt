package com.example.mathhelper

import android.graphics.Color
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import android.content.Context
import com.example.mathhelper.models.Score

abstract class AbstractActivity : AppCompatActivity() {

    protected lateinit var buttonList: ArrayList<Button>
    protected var score: Score = Score(this)
    protected var counter: Int = 0

    protected fun setOnClickListeners() {
        for (i in buttonList.indices) {
            buttonList[i].setOnClickListener {
                if (buttonList[i] != buttonList[0]) {
                    buttonList[i].setBackgroundColor(Color.RED)
                    counter = 0
                } else{
                    counter++
                    score.checkScore(counter.toString())
                }
                buttonList[0].setBackgroundColor(Color.GREEN)
            }
        }
    }
}