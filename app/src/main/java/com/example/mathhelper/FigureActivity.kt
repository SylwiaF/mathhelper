package com.example.mathhelper

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import com.example.mathhelper.number.OperationGenerator

class FigureActivity : AbstractActivity() {

    private lateinit var firstFigure: TextView
    private lateinit var secondFigure: TextView
    private lateinit var operationFigure: TextView
    private lateinit var actualScore: TextView
    private lateinit var bestScore: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_figure)

        val userChoiceRange = intent.getIntExtra("range", 100)
        val userChoiceOperations = intent.getStringExtra("operations")
        counter = intent.getIntExtra("counter", 0)

        val generator = OperationGenerator(userChoiceRange)

        if (userChoiceOperations == "+-") {
            generator.setOperationMatrix(mutableListOf("+", "-"))
        }

        val operation = generator.generate()

        buttonList = arrayListOf(
            findViewById(R.id.button1_figure),
            findViewById(R.id.button2_figure),
            findViewById(R.id.button3_figure),
            findViewById(R.id.button4_figure)
        )
        buttonList.shuffle()

        actualScore = findViewById(R.id.actual_figure)
        actualScore.text = counter.toString()
        bestScore = findViewById(R.id.best_figure)
        bestScore.text = score.maxResult()

        firstFigure = findViewById(R.id.first_figure)
        secondFigure = findViewById(R.id.second_figure)

        operationFigure = findViewById(R.id.operation_figure)
        operationFigure.text = operation.operator


        if (operation.x == null) {
            firstFigure.visibility = View.INVISIBLE
        } else {
            firstFigure.text = operation.x.toString()
        }

        if (operation.y == null) {
            secondFigure.visibility = View.INVISIBLE
        } else {
            secondFigure.text = operation.y.toString()
        }

        buttonList[0].text = operation.solution.toString()
        for (i in 1 until buttonList.size) {
            "${operation.answers[i - 1]}".also { buttonList[i].text = it }
        }

        setOnClickListeners()
        findViewById<Button>(R.id.start_figure).setOnClickListener {
            finish()
            intent.putExtra("counter", counter)
            startActivity(intent)
        }
    }

}