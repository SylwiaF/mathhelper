package com.example.mathhelper.shape

import com.example.mathhelper.R
import com.example.mathhelper.models.ShapeCommons
import com.example.mathhelper.models.ShapeInterface
import com.example.mathhelper.shape.rectangle.*

class Rectangle : AbstractShape() {
    init {
        taskDrawing = R.drawable.rectangle
    }

    private var tasksArray = arrayOf(
        AreaFromSlides(),
        AreaFromPerimeterAndSlide(),
        PerimeterFromSldes(),
        PerimeterFromAreaAndSlide(),
        SlideFromAreaAndSlide(),
        SlideFromPerimeterAndSlide()
    )

    override fun generateTask(): ShapeInterface {
        tasksArray.shuffle()
        tasksArray[0].fill(this)

        ShapeCommons().buildAnswers(this, 3)

        return this
    }

}