package com.example.mathhelper.shape.square

import com.example.mathhelper.models.ShapeCommons
import com.example.mathhelper.models.SquareTaskInterface
import com.example.mathhelper.shape.Square

class AreaFromSide : SquareTaskInterface {
    override fun fill(item: Square) {
        val a = ShapeCommons().getRandomPow(1,item.range)
        item.factorA = "a = $a"
        item.taskSolution = a * a
        item.text = "Oblicz pole kwadratu, znając długość jego boku"
    }
}