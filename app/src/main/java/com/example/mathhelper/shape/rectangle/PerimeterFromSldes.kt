package com.example.mathhelper.shape.rectangle

import com.example.mathhelper.models.RectangleTaskInterface
import com.example.mathhelper.shape.Rectangle
import kotlin.random.Random

class PerimeterFromSldes: RectangleTaskInterface {
    override fun fill(item: Rectangle) {
        val a = Random.nextInt(1, item.range / 2 - 2)
        val b = Random.nextInt(1, item.range/2 - a)
        item.factorA = "a = $a"
        item.factorB = "b = $b"
        item.taskSolution = (a + b) * 2
        item.text = "Oblicz obwód prostokata, znając długość jego boków"
    }
}