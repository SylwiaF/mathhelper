package com.example.mathhelper.shape

import com.example.mathhelper.models.ShapeCommons
import com.example.mathhelper.models.ShapeInterface
import com.example.mathhelper.shape.triangle.*

class Triangle : AbstractShape() {

    private var tasksArray = arrayOf(
        T2HightFromSides(),
        T2PerimeterFromSides(),
        T2PerimeterFromSides()
    )

    override fun generateTask(): ShapeInterface {
        tasksArray.shuffle()
        tasksArray[0].fill(this)

        ShapeCommons().buildAnswers(this, 3)

        return this
    }

}