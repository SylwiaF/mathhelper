package com.example.mathhelper.shape.rectangle

import com.example.mathhelper.models.RectangleTaskInterface
import com.example.mathhelper.models.ShapeCommons
import com.example.mathhelper.shape.Rectangle

class AreaFromPerimeterAndSlide : RectangleTaskInterface {
    override fun fill(item: Rectangle) {
        val a = ShapeCommons().getRandomPow(1, item.range)
        val b = ShapeCommons().getRandomPow(1, item.range)
        item.factorA = "a = $a"
        item.factorB = "obwód = ${(a + b) * 2}"
        item.taskSolution = a * b
        item.text = "Oblicz pole prostokata, znając długość jednego boku i jego obwód"
    }
}