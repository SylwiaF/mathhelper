package com.example.mathhelper.shape.square

import com.example.mathhelper.models.SquareTaskInterface
import com.example.mathhelper.shape.Square
import kotlin.random.Random

class SlideFromPerimeter : SquareTaskInterface {
    override fun fill(item: Square) {
        val a = Random.nextInt(1, item.range / 4)
        item.factorA = "obwód = ${4 * a}"
        item.taskSolution = a * a
        item.text = "Oblicz pole kwadratu, znając jego obwód"
    }
}