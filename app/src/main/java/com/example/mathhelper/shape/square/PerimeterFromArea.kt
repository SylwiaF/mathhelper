package com.example.mathhelper.shape.square

import com.example.mathhelper.models.ShapeCommons
import com.example.mathhelper.models.SquareTaskInterface
import com.example.mathhelper.shape.Square

class PerimeterFromArea : SquareTaskInterface {
    override fun fill(item: Square) {
        val a = ShapeCommons().getRandomPow(1,item.range)
        item.factorA = "pole = ${a * a}"
        item.taskSolution = 4 * a
        item.text = "Oblicz obwód kwadratu, znając jego pole"
    }
}