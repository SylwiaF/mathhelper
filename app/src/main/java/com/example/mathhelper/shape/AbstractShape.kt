package com.example.mathhelper.shape

import com.example.mathhelper.models.ShapeInterface
import com.example.mathhelper.shape.rectangle.AreaFromSlides

abstract class AbstractShape(val range: Int = 100) : ShapeInterface {
    private var taskAnswers: MutableList<Int> = ArrayList()

    var text: String = ""
    var taskSolution: Int = 0
    var taskDrawing: Int = 0

    var factorA: String = ""
    var factorB: String = ""

    override fun getDrawing(): Int {
        return taskDrawing
    }

    override fun getFirstFactor(): String {
        return factorA
    }

    override fun getSecondFactor(): String {
        return factorB
    }

    override fun getTaskText(): String {
        return text
    }

    override fun getSolution(): Int {
        return taskSolution
    }

    override fun getAnswers(): MutableList<Int> {
        return taskAnswers
    }

    override fun setAnswers(answers: MutableList<Int>) {
        taskAnswers = answers
    }
}