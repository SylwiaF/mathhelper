package com.example.mathhelper.shape.square

import com.example.mathhelper.models.ShapeCommons
import com.example.mathhelper.models.SquareTaskInterface
import com.example.mathhelper.shape.Square

class SlideFromArea : SquareTaskInterface {
    override fun fill(item: Square) {
        val a = ShapeCommons().getRandomPow(1,item.range)
        item.factorA = "pole = ${a * a}"
        item.taskSolution = a
        item.text = "Oblicz długość boku kwadratu, znając jego pole"
    }
}