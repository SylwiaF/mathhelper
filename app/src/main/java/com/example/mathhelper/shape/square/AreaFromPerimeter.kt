package com.example.mathhelper.shape.square

import com.example.mathhelper.models.ShapeCommons
import com.example.mathhelper.models.SquareTaskInterface
import com.example.mathhelper.shape.Square

class AreaFromPerimeter : SquareTaskInterface {
    override fun fill(item: Square) {
        val a = ShapeCommons().getRandomPow(1,item.range)
        item.factorA = "obwód = ${4 * a}"
        item.taskSolution = a * a
        item.text = "Oblicz pole kwadratu, znając jego obwód"
    }
}