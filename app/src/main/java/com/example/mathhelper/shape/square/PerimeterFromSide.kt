package com.example.mathhelper.shape.square

import com.example.mathhelper.models.ShapeCommons
import com.example.mathhelper.models.SquareTaskInterface
import com.example.mathhelper.shape.Square

class PerimeterFromSide : SquareTaskInterface {
    override fun fill(item: Square) {
        val a = ShapeCommons().getRandomPow(1,(item.range/4).toInt())
        item.factorA = "a = $a"
        item.taskSolution = 4 * a
        item.text = "Oblicz obwód kwadratu, znając długość jego boku"
    }
}