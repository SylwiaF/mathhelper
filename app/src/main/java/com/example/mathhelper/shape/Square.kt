package com.example.mathhelper.shape

import com.example.mathhelper.R
import com.example.mathhelper.models.ShapeCommons
import com.example.mathhelper.models.ShapeInterface
import com.example.mathhelper.shape.square.*

class Square : AbstractShape() {
    init {
        taskDrawing = R.drawable.square
    }

    private var tasksArray = arrayOf(
        AreaFromSide(),
        AreaFromPerimeter(),
        PerimeterFromSide(),
        PerimeterFromArea(),
        SlideFromArea(),
        SlideFromPerimeter()
    )

    override fun generateTask(): ShapeInterface {
        tasksArray.shuffle()
        tasksArray[0].fill(this)

        ShapeCommons().buildAnswers(this, 3)

        return this
    }
}