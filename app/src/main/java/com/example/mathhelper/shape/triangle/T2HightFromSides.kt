package com.example.mathhelper.shape.triangle

import com.example.mathhelper.R
import com.example.mathhelper.models.ShapeCommons
import com.example.mathhelper.models.TriangleTaskInterface
import com.example.mathhelper.shape.Triangle

class T2HightFromSides : TriangleTaskInterface {
    override fun fill(item: Triangle) {
        val triplet = ShapeCommons().randomPythagoreanTriplets(20)

        item.taskDrawing = R.drawable.triangle2
        item.factorA = "a = ${triplet.a * 2}"
        item.factorB = "b = ${triplet.c}"
        item.taskSolution = triplet.b
        item.text = "Oblicz wysokość trójkąta równoramiennego, znając długości jego boków"

    }
}