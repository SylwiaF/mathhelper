package com.example.mathhelper.shape.rectangle

import com.example.mathhelper.models.RectangleTaskInterface
import com.example.mathhelper.models.ShapeCommons
import com.example.mathhelper.shape.Rectangle

class AreaFromSlides : RectangleTaskInterface {
    override fun fill(item: Rectangle) {
        val a = ShapeCommons().getRandomPow(1,item.range)
        val b = ShapeCommons().getRandomPow(1,item.range)
        item.factorA = "a = $a"
        item.factorB = "b = $b"
        item.taskSolution = a * b
        item.text = "Oblicz pole prostokata, znając długość jego boków"
    }
}