package com.example.mathhelper.shape.triangle

import com.example.mathhelper.R
import com.example.mathhelper.models.ShapeCommons
import com.example.mathhelper.models.TriangleTaskInterface
import com.example.mathhelper.shape.Triangle

class T2PerimeterFromSides : TriangleTaskInterface {
    override fun fill(item: Triangle) {
        val a = ShapeCommons().getRandomPow(1, item.range / 2) * 2
        val h = ShapeCommons().getRandomPow(1, item.range)
        item.taskDrawing = R.drawable.triangle2
        item.factorA = "a = $a"
        item.factorB = "h = $h"
        item.taskSolution = (a * h) / 2
        item.text = "Oblicz pole trójkąta równoramiennego, znając długość podstawy oraz wysokość"
    }

}